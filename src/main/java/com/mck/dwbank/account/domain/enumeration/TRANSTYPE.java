package com.mck.dwbank.account.domain.enumeration;

/**
 * The TRANSTYPE enumeration.
 */
public enum TRANSTYPE {
    CREDIT, DEBIT
}
