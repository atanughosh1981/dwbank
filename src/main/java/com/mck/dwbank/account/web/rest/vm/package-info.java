/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.dwbank.account.web.rest.vm;
