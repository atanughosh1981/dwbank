import { NgModule } from '@angular/core';

import { DwbankSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [DwbankSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [DwbankSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class DwbankSharedCommonModule {}
