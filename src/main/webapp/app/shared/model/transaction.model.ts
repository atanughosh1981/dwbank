import { Moment } from 'moment';

export const enum TRANSTYPE {
    CREDIT = 'CREDIT',
    DEBIT = 'DEBIT'
}

export interface ITransaction {
    id?: number;
    transactionDate?: Moment;
    transactionRemark?: string;
    transactionType?: TRANSTYPE;
    availableBalance?: number;
    firstName?: string;
    lastName?: string;
}

export class Transaction implements ITransaction {
    constructor(
        public id?: number,
        public transactionDate?: Moment,
        public transactionRemark?: string,
        public transactionType?: TRANSTYPE,
        public availableBalance?: number,
        public firstName?: string,
        public lastName?: string
    ) {}
}
