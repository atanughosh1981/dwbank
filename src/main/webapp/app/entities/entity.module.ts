import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { DwbankBankAccountModule } from './bank-account/bank-account.module';
import { DwbankTransactionModule } from './transaction/transaction.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        DwbankBankAccountModule,
        DwbankTransactionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DwbankEntityModule {}
