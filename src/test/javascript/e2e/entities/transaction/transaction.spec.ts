import { browser } from 'protractor';
import { NavBarPage } from './../../page-objects/jhi-page-objects';
import { TransactionComponentsPage, TransactionUpdatePage } from './transaction.page-object';

describe('Transaction e2e test', () => {
    let navBarPage: NavBarPage;
    let transactionUpdatePage: TransactionUpdatePage;
    let transactionComponentsPage: TransactionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Transactions', () => {
        navBarPage.goToEntity('transaction');
        transactionComponentsPage = new TransactionComponentsPage();
        expect(transactionComponentsPage.getTitle()).toMatch(/Transactions/);
    });

    it('should load create Transaction page', () => {
        transactionComponentsPage.clickOnCreateButton();
        transactionUpdatePage = new TransactionUpdatePage();
        expect(transactionUpdatePage.getPageTitle()).toMatch(/Create or edit a Transaction/);
        transactionUpdatePage.cancel();
    });

    it('should create and save Transactions', () => {
        transactionComponentsPage.clickOnCreateButton();
        transactionUpdatePage.setTransactionDateInput('2000-12-31');
        expect(transactionUpdatePage.getTransactionDateInput()).toMatch('2000-12-31');
        transactionUpdatePage.setTransactionRemarkInput('transactionRemark');
        expect(transactionUpdatePage.getTransactionRemarkInput()).toMatch('transactionRemark');
        transactionUpdatePage.transactionTypeSelectLastOption();
        transactionUpdatePage.setAvailableBalanceInput('5');
        expect(transactionUpdatePage.getAvailableBalanceInput()).toMatch('5');
        transactionUpdatePage.setFirstNameInput('firstName');
        expect(transactionUpdatePage.getFirstNameInput()).toMatch('firstName');
        transactionUpdatePage.setLastNameInput('lastName');
        expect(transactionUpdatePage.getLastNameInput()).toMatch('lastName');
        transactionUpdatePage.save();
        expect(transactionUpdatePage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});
