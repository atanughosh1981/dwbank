import { element, by, promise, ElementFinder } from 'protractor';

export class TransactionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    title = element.all(by.css('jhi-transaction div h2#page-heading span')).first();

    clickOnCreateButton(): promise.Promise<void> {
        return this.createButton.click();
    }

    getTitle(): any {
        return this.title.getText();
    }
}

export class TransactionUpdatePage {
    pageTitle = element(by.id('jhi-transaction-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    transactionDateInput = element(by.id('field_transactionDate'));
    transactionRemarkInput = element(by.id('field_transactionRemark'));
    transactionTypeSelect = element(by.id('field_transactionType'));
    availableBalanceInput = element(by.id('field_availableBalance'));
    firstNameInput = element(by.id('field_firstName'));
    lastNameInput = element(by.id('field_lastName'));

    getPageTitle() {
        return this.pageTitle.getText();
    }

    setTransactionDateInput(transactionDate): promise.Promise<void> {
        return this.transactionDateInput.sendKeys(transactionDate);
    }

    getTransactionDateInput() {
        return this.transactionDateInput.getAttribute('value');
    }

    setTransactionRemarkInput(transactionRemark): promise.Promise<void> {
        return this.transactionRemarkInput.sendKeys(transactionRemark);
    }

    getTransactionRemarkInput() {
        return this.transactionRemarkInput.getAttribute('value');
    }

    setTransactionTypeSelect(transactionType): promise.Promise<void> {
        return this.transactionTypeSelect.sendKeys(transactionType);
    }

    getTransactionTypeSelect() {
        return this.transactionTypeSelect.element(by.css('option:checked')).getText();
    }

    transactionTypeSelectLastOption(): promise.Promise<void> {
        return this.transactionTypeSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }
    setAvailableBalanceInput(availableBalance): promise.Promise<void> {
        return this.availableBalanceInput.sendKeys(availableBalance);
    }

    getAvailableBalanceInput() {
        return this.availableBalanceInput.getAttribute('value');
    }

    setFirstNameInput(firstName): promise.Promise<void> {
        return this.firstNameInput.sendKeys(firstName);
    }

    getFirstNameInput() {
        return this.firstNameInput.getAttribute('value');
    }

    setLastNameInput(lastName): promise.Promise<void> {
        return this.lastNameInput.sendKeys(lastName);
    }

    getLastNameInput() {
        return this.lastNameInput.getAttribute('value');
    }

    save(): promise.Promise<void> {
        return this.saveButton.click();
    }

    cancel(): promise.Promise<void> {
        return this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}
